from django.contrib import admin
from receipts.models import ExpenseCategory, Account, Receipt

# Register your models here.


class ExpenseCategoryAdmin(admin.ModelAdmin):
    pass


class AccountCategoryAdmin(admin.ModelAdmin):
    pass


class ReceiptCategoryAdmin(admin.ModelAdmin):
    pass


admin.site.register(ExpenseCategory)
admin.site.register(Account)
admin.site.register(Receipt)
