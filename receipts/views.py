from django.shortcuts import render
from django.views.generic.list import ListView
from .models import Receipt
from django.views.generic.base import RedirectView

# Create your views here.


class ReceiptListView(ListView):
    model = Receipt
    template_name = "receipt_list.html"


class ReceiptRedirectView(RedirectView):
    url = "/receipts/"
    pattern_name = "home"
